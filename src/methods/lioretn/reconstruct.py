"""The main file for the reconstruction.
This file should NOT be modified excecdpt the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""

import numpy as np
from src.methods.lioretn.methods.Bicubic import Bicubic
from src.methods.lioretn.methods.Brovey import Brovey
from src.methods.lioretn.methods.SFIM import SFIM
from src.methods.lioretn.methods.PCA import PCA
from src.methods.lioretn.methods.Wavelet import Wavelet
from src.methods.lioretn.methods.IHS import IHS
from src.methods.lioretn.methods.GFPCA import GFPCA
from src.methods.lioretn.methods.GS import GS
from src.methods.lioretn.methods.GSA import GSA
from src.methods.lioretn.methods.MTF_GLP import MTF_GLP
from src.methods.lioretn.methods.MTF_GLP_HPM import MTF_GLP_HPM
from src.methods.lioretn.methods.PNN import PNN
from src.methods.lioretn.methods.PanNet import PanNet
from src.methods.lioretn.methods.CNMF import CNMF
from src.methods.lioretn.wavelengths import get_wavelengths_repartition, get_best_matching_wavelengths

def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs data fusion of the three acquisitions according to the method and the approach chosen
    (modify directly within the function).

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    # Choose the pansharpening technique you want to use
    # Choose among : Bicubic, Brovey, SFIM, PCA, Wavelet, IHS, GFPCA, GS, GSA, MTF_GLP, MTF_GLP_HPM, PNN, PanNet, CNMF
    method = CNMF

    # Choose what approach you want to have (direct, multidirect, throughMRMS)
    # For 'throughMRMS' method, you can change the minimum value of absorption in wavelengths.py
    approach = 'throughMRMS'
    pan = np.insert(pan, 0, pan[0, :], axis=0)[:, :, np.newaxis] # A pixel was "missing" in the panchromatic image

    all_bands = ms2.shape[2]
    medium_bands = ms1.shape[2]

    #### DIRECT PANSHARPENING METHOD ####
    if approach == 'direct':
        return method(pan, ms2)[1:, :, :] # We apply directly the pansharpening method

    #### MULTIPLE DIRECT PANSHARPENING METHOD ####
    if approach == 'multidirect':
        # Getting the LRMS wavelengths which maximizes the MRMS absorption value
        best_index = get_best_matching_wavelengths()
        ms2_rev = np.delete(ms2, best_index, axis=2) # Delete the 9 selected wavelengths from LRMS

        # Direct pansharpening from LRMS to Pan
        ms2_pan = method(pan, ms2_rev)[1:, :, :]
        # Direct pansharpening from MRMS to Pan
        ms1_pan = method(pan, ms1)[1:, :, :]

        # Reconstruction of the result
        result = np.zeros((pan.shape[0]-1, pan.shape[1], all_bands))
        counter = [0, 0]
        for b in range(all_bands):
            if b in best_index: # MRMS to Pan bands
                result[:, :, b] = ms1_pan[:, :, counter[1]]
                counter[1] += 1
            else: # LRMS to Pan bands
                result[:, :, b] = ms2_pan[:, :, counter[0]]
                counter[0] += 1

        return result

    #### THROUGH MRMS METHOD ####
    if approach == 'throughMRMS':
        # Get the LRMS wavelengths sorted to their corresponding MRMS bands (and the ones which does not suit any band)
        index = get_wavelengths_repartition()
        first_pan_ms = [[] for b in range(medium_bands+1)] # List of the 10 set of wavelengths
        for b in range(len(index)):
            first_pan_ms[index[b]].append(ms2[:, :, b:b+1])

        first_pan_ms = [np.concatenate(first_pan_ms[b], axis=2) for b in range(medium_bands+1)]
        # First pansharpening to medium spatial resolution using the determined associated band of MRMS
        second_pan_ms = [method(ms1[:,:,b:b+1], first_pan_ms[b]) for b in range(medium_bands)]
        # Second pansharpening to fine scale using the first pansharpening results
        final_pan_ms = [method(pan, second_pan_ms[b])[1:, :, :] for b in range(medium_bands)]
        final_pan_ms.append(method(pan, first_pan_ms[medium_bands])[1:, :, :])

        # Build the result
        result = np.zeros((pan.shape[0]-1, pan.shape[1], all_bands))
        counter = [0 for b in range(medium_bands+1)]
        for b in range(all_bands):
            i = index[b]
            result[:, :, b] = final_pan_ms[i][:,:,counter[i]]
            counter[i] += 1

        return result

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
