# -*- coding: utf-8 -*-
"""
License: MIT
@author: gaj
E-mail: anjing_guo@hnu.edu.cn
Paper References:
    [1] W. Liao et al., "Two-stage fusion of thermal hyperspectral and visible RGB image by PCA and guided filter," 
        2015 7th Workshop on Hyperspectral Image and Signal Processing: Evolution in Remote Sensing (WHISPERS), Tokyo, 2015, pp. 1-4.
"""

import numpy as np
from sklearn.decomposition import PCA as princomp
from cv2.ximgproc import guidedFilter
from src.methods.lioretn.utils import upsample_interp23

def GFPCA(pan, ms):
    M, N, c = pan.shape
    m, n, C = ms.shape
    ratio = int(M/m)

    # Check that the format of pan and ms image are similar
    assert int(np.round(M/m)) == int(np.round(N/n))

    p = princomp(n_components=C)
    pca_ms = p.fit_transform(np.reshape(ms, (m*n, C)))
    
    pca_ms = np.reshape(pca_ms, (m, n, C))
    
    pca_ms = upsample_interp23(pca_ms, ratio)
    
    gp_ms = []
    for i in range(C):
        temp = guidedFilter(np.float32(pan), np.float32(np.expand_dims(pca_ms[:, :, i], -1)), 8, eps = 0.001**2)
        temp = np.expand_dims(temp ,axis=-1)
        gp_ms.append(temp)
        
    gp_ms = np.concatenate(gp_ms, axis=-1)
    
    I_GFPCA = p.inverse_transform(gp_ms)
    
    # Adjustment
    I_GFPCA[I_GFPCA<0]=0
    I_GFPCA[I_GFPCA>1]=1
    
    return np.float16(I_GFPCA)