"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
import src.methods.Samanos.function as f


def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    # Performing the reconstruction with the upscale of ms1
    # return f.evolve_ms1(ms1,pan.shape,ponder=False)

    # Performing the reconstruction with the pondered sum of all channels of ms1
    # return f.evolve_ms1(ms1,pan.shape,ponder=True)

    # Performing the reconstruction with the pondered sum of all channels but 1 with ms1
    # return f.evolve_ms1(ms1,pan.shape,remove=True,remove_channel=7,ponder=True)

    # Performing the reconstruction with the upscale of ms2
    # return f.upscale_ms2(ms2,pan.shape)

    # Performing the reconstruction with the upscale of pan
    # return f.rescale_pan(pan)

    # Performing the reconstruction with the pondered sum of pan
    # return f.evolve_pan(pan)

    # Performing the reconstruction with the average of the three best methods
    # ponder = (1,2,1)
    # return f.mean_three_upscales(pan,ms1,ms2,ponder=ponder)

    # Performing the reconstruction with the rgb2hsv method on the ms1 image
    # return f.improve_ms1(ms1,pan)

    # Performing the reconstruction with the rgb2hsv method on the ms2 image
    # return f.improve_ms2(ms2,pan)

    # Performing the reconstruction with a ponderated version of the spectrum
    # return f.pan_ponder_spectrum(ms1,pan,ms1=True)

    # Performing the reconstruction with the mean of the 3 images at each frequency
    return f.evolve_linear_combination(pan,ms1,ms2)

####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
